1. STL files can be printed directly in 3D

Recommended nozzle size: 0.4mm

Recommended floor height: 0.2mm

ABS or nylon is recommended for better strength

2. The SolidWorks file is the design source file

Some datum planes are missing, which does not affect editing

About Mouse

Some parts need to print more than one (mirror image may be required)

Other connection modes can be used between different modules

At present, only type-C mother seat installation position is designed for key wire separation

It is recommended to confirm whether the size is appropriate before printing the upper cover

There are certain requirements for the size of the mouse motherboard

V3 has been updated. As some parts are large, please ensure that the printer range is about 300 * 300. It is not recommended to split the main body, panel and bracket for printing.

The main body has been designed to reduce the support, so be sure to support it (note! The front key support is still difficult to remove)

It is recommended to print the main body first. The author has measured that the ABS material 235 ° / 100 ° can be printed perfectly (CREALITY  cr10smart)

After the main body is printed, a more selective mouse is needed to adjust the position and size of the sensor opening

The fixing screws are M2.5 * 6 * 5 flat head screws and M3 * 16 umbrella head screws

The magnet is 10 * 5 * 3mm

It is recommended to use thunder snake shaft for shaft body (other shaft bodies may need to be polished)

To prepare super glue, the author uses ergo 5800 (502 is not sure whether the strength is enough)

All documents and installation steps have been provided

Technical support cannot be provided separately

Please refer to for specific installation steps https://www.bilibili.com/video/BV1Nv411K7fq

More questions can be discussed in QQ group chat 303929333